## Playbook to configure secure self-hosted VPN

### Overview

This is an Ansible Playbook to [Wolfgang's VPN tutorial](https://www.youtube.com/watch?v=gxpX_mubz2A) as well as his [ssh honeypot tutorial](https://www.youtube.com/watch?v=SKhKNUo6rJU). 

!The latter is work in progress and currently resides on its own feature branch still!

The playbook follows the steps in the videos very closely. Feel free to suggest improvements and make amendments. 

The user password for the remote machine is 'password' and not hashed. That is on purpose. We disallow root login and password login via the playbook. Hence, it is only possible to login using the private key file. If an attacker gets that far you have messed up big time anyway. 

### How to

The instructions to run the playbook are in the top comments of the playbook itself (deployvpn.yml).

You should define your host(s) in the hosts file and specify the name of the host group to the playbook under hosts. In this case I calles it eu-west-linode. You can also specify several ip addresses for this group or create and target multiple host groups to provision.

The required variables are in a YAML file in vars/host_vars.yml. You need to change them accordingly. Most things are fine to leave as they are but you definitely want to change the public key to your public key as you will otherwise find yourself locked out of your box - we have password and root login disabled.  

This was tested against teh same setup as in the videos: A nano-sized Ubuntu 20.04LTS box from Linode on Uk servers.

### Caveats

Linode currently closes the standard email ports by default. In order to receive emails about the auto upgrades you'll have to create a support ticket with Linode to allow the ports to be opened (by default). I am not sure whether it is also possible to change this manually. 

In principle this should work against any box running Ubuntu 20.04, but that has not been tested and the fact that I cannot think of why it shouldn't work is a very poor argument to assume it will work.

